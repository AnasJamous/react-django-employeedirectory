**About**
Employees Directory APP build in Django-framework and ReactJS

**SetUp**
- Download / Clone repo.
- Create and activate a virtual environment (steps for creating new virtual env in section bellow)
- Navigate to the `employee_directory` folder that contains `manage.py` and run `python manage.py runserver`
- At the root folder that holds `package.json` run `npm i`
- At the root folder to start react app run `npm run dev`
- Now you should be able to see the App at your fav browser.

**SetUp A Virtual Environment**
 There are many ways to create new v-env but the following is the best practice to do so:
 - make sure you have Python installed on your OS.
 - open the link and install Anaconda according to your OS `https://docs.anaconda.com/anaconda/install/`
 - open up the anaconda terminal.
    1. Create a new environment
        `conda create -n djangoenv python=3.6 anaconda`
    2. Activate the new environment
        `conda activate djangoenv`
 - open the project in your preferred IDE and open the terminal:
    and **make sure** to set you **interpreter** to the same v-env you created.
    
 - Now you are at the root folder where package.json exist.
 - In the terminal window run: `pip install -r requirements.txt`
 - We are done setting up the v-env now continue steps in the Setup section.
