from django.contrib import admin
from django.urls import path, include

"""
    employee_directory URL Configuration
"""

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('', include('frontend.urls')),
    path('', include('employee.urls')),
    path('', include('accounts.urls')),
]
