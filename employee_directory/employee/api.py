from rest_framework import viewsets, permissions

from .models import Employee
from .serializers import EmployeeSerializers


'''
    ViewSet it allows to create a full CRUD API without having to specify explicit method for
    the functionality.
'''


# Employee ViewSet:
class EmployeeViewSet(viewsets.ModelViewSet):
    permission_classes = [
        # make the return of employees list valid only for the auth users
        permissions.IsAuthenticated
    ]

    serializer_class = EmployeeSerializers

    # Return list of employees for the a particular user
    def get_queryset(self):
        return self.request.user.employees.all()

    # Allows to save the employee owner when we create a new employee
    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
