from django.db import models

# User Model comes by default with Django
from django.contrib.auth.models import User


#  Employee Model:
class Employee(models.Model):
    name = models.CharField(max_length=40)
    job_title = models.CharField(max_length=30)
    bio = models.CharField(max_length=300, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    email = models.EmailField(max_length=100, unique=True)
    owner = models.ForeignKey(User, related_name="employees", on_delete=models.CASCADE, null=True)
