from rest_framework import serializers

from .models import Employee


# Lead Serializers:
class EmployeeSerializers(serializers.ModelSerializer):
    class Meta:
        model = Employee
        # __all__ refers to all the fields in the model Employee
        fields = '__all__'
