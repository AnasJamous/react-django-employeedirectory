// This is Just a Meeting place for all the reducers we have in the App
import { combineReducers } from 'redux'

// MY Reducers
import employees from './employees'
import errors from './errors'
import messages from './messages'
import auth from './auth'

export default combineReducers({
    auth,
    errors,
    messages,
    employees,
})