import {
    GET_EMPLOYEES,
    DELETE_EMPLOYEE,
    ADD_EMPLOYEE,
    GET_RANDOM_EMPLOYEES,
    UPDATE_EMPLOYEE,
    EMPLOYEE_INFO,
} from '../actions/types'

const initialState = {
    employees: [],
    randomEmployees:[],
    employeeInfo: {}
}

export default function(state = initialState, action) {
    switch(action.type) {
        case GET_EMPLOYEES:
            return {
                ...state,
                employees: action.payload
            }
        case DELETE_EMPLOYEE:
            return {
                ...state,
                employees: state.employees.filter(emp => emp.id != action.payload)
            }
        case UPDATE_EMPLOYEE:
            const updateState = state.employees.filter(emp => emp.id != action.payload.id)
            console.log('update from state reducer',updateState)
            return {
                ...state,
                employees: [...updateState, action.payload]
            }
        case ADD_EMPLOYEE:
            return {
                ...state,
                employees: [...state.employees, action.payload]
            }
        case GET_RANDOM_EMPLOYEES:
            return {
                ...state,
                randomEmployees: action.payload
            }
        case EMPLOYEE_INFO:
            return {
                ...state,
                employeeInfo: action.payload
            }
        default:
            return state
    }
}