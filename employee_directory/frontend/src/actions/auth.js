import axios from 'axios'
import { returnErrors } from './messages'

import {
    USER_LOADED,
    USER_LOADING,
    AUTH_ERROR,
    LOGIN_FAIL,
    LOGIN_SUCCESS,
    LOGOUT_SUCCESS,
    REGISTER_SUCCESS,
    REGISTER_FAIL
} from './types'


// Check the token & load the user
export const loadUser = () => (dispatch, getState) => {
    // Set User Loading
    dispatch({ type: USER_LOADING });

    // Call helper func to set the config
    const config = tokenConfig(getState)

    axios.get('/api/auth/user', config)
        .then(res => {
            dispatch({
                type: USER_LOADED,
                payload: res.data
            })
        })
        .catch(err => {
            dispatch(returnErrors(err.response.data, err.response.status));
            dispatch({
                type: AUTH_ERROR
            })
        })
}

// Login User
export const userLogin = (username, password) => dispatch => {
    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }

    // Request body | turn it into JOSN
    const body = JSON.stringify({username, password})

    axios.post('/api/auth/login',body, config)
        .then(res => {
            console.log('User Login Response', res.data)
            dispatch({
                type: LOGIN_SUCCESS,
                payload: res.data
            })
        })
        .catch(err => {
            dispatch(returnErrors(err.response.data, err.response.status));
            dispatch({
                type: LOGIN_FAIL
            })
        })
}

// Register User
export const registerUser = ({username, email, password}) => dispatch => {
    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    // Request body | turn it into JOSN
    const body = JSON.stringify({username,email, password})

    axios.post('/api/auth/register',body, config)
        .then(res => {
            dispatch({
                type: REGISTER_SUCCESS,
                payload: res.data
            })
        })
        .catch(err => {
            dispatch(returnErrors(err.response.data, err.response.status));
            dispatch({
                type: REGISTER_FAIL
            })
        })
}

// Logout User
export const logout = () => (dispatch, getState) => {
    // Call helper func to set the config
    const config = tokenConfig(getState)

    // Request Body
    const body = null

    axios.post('/api/auth/logout/',body, config)
        .then(res => {
            dispatch({
                type: LOGOUT_SUCCESS
            })
        })
        .catch(err => {
            dispatch(returnErrors(err.response.data, err.response.status));
        })
}

// Setup Cofig with token - helper function
export const tokenConfig = getState => {
    // Get token from state
    const token = getState().auth.token

    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }

    // If token ? Add to headers
    if (token) {
        config.headers['Authorization'] = `Token ${token}`
    }
    return config
}