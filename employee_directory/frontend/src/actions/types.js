// Employees Types
export const ADD_EMPLOYEE = 'ADD_EMPLOYEE'
export const GET_EMPLOYEES = 'GET_EMPLOYEES'
export const DELETE_EMPLOYEE = 'DELETE_EMPLOYEE'
export const UPDATE_EMPLOYEE = 'UPDATE_EMPLOYEE'
export const GET_RANDOM_EMPLOYEES = 'GET_RANDOM_EMPLOYEES'
export const EMPLOYEE_INFO = 'EMPLOYEE_INFO'

// Errors Types
export const GET_ERRORS = 'GET_ERRORS'
export const CREATE_MESSAGE = 'CREATE_MESSAGE'

// Authentication Types
export const AUTH_ERROR = 'AUTH_ERROR'
export const USER_LOADED = 'USER_LOADED'
export const USER_LOADING = 'USER_LOADING'

// Login, Register, Logout Types
export const LOGIN_FAIL = 'LOGIN_FAIL'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const REGISTER_FAIL = 'REGISTER_FAIL'
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
