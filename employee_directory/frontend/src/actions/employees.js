import axios from 'axios'

import { tokenConfig } from './auth'

import { createMessage, returnErrors } from './messages'
import { GET_EMPLOYEES, DELETE_EMPLOYEE, ADD_EMPLOYEE, GET_RANDOM_EMPLOYEES, UPDATE_EMPLOYEE,EMPLOYEE_INFO } from './types'

// Get Random Employees using --> https://randomuser.me/
export const getRandomEmployees = () => dispatch => {

    // Set Headers to allow origin
    const config = {
        headers: {"Access-Control-Allow-Origin": "*"}
    }

    axios.get('https://randomuser.me/api/?results=4513', config)
        .then(res => {
            dispatch({
                type: GET_RANDOM_EMPLOYEES,
                payload: res.data.results
            })
        })
        .catch(err => {
            dispatch(returnErrors(err.response.data, err.response.status))
        })
}


// Get Employees
export const getEmployees = () => (dispatch, getState) => {
    // Call helper func to set the config
    const token = tokenConfig(getState)

    axios.get('/api/employees/', token)
        .then(res => {
            dispatch({
                type: GET_EMPLOYEES,
                payload: res.data
            });
        })
        .catch(err =>
            dispatch(
                returnErrors(
                    err.response.data,
                    err.response.status
                )
            )
        )
}

// Delete Employee Visually
export const deleteEmployeeVisually = () => dispatch => {
    dispatch(
        createMessage(
            { deleteEmployee: 'Employee Deleted' }
        )
    )
}

// Delete Employee
export const deleteEmployee = id => (dispatch, getState) => {
    // Call helper func to set the config
    const token = tokenConfig(getState)

    axios.delete(`/api/employees/${id}/`, token)
        .then(res => {
            dispatch(
                createMessage(
                    { deleteEmployee: 'Employee Deleted' }
                )
            )
            dispatch({
                type: DELETE_EMPLOYEE,
                payload: id
            });
        })
        .catch(err => console.log(err))
}

// Update Employee Visually
export const UpdateEmployeeVisually = () => dispatch => {
    dispatch(
        createMessage(
            { updateEmployee: 'Employee Updated' }
        )
    )
}

// Update Employee
export const updateEmployee = (id, updatedEmp) => (dispatch, getState) => {

    // Call helper func to set the config
    const token = tokenConfig(getState)

    axios.put(`/api/employees/${id}/`,updatedEmp, token)
        .then(res => {
            dispatch(
                createMessage(
                    { updateEmployee: 'Employee Updated' }
                )
            )
            dispatch({
                type: UPDATE_EMPLOYEE,
                payload: res.data
            });
        })
        .catch(err =>
            dispatch(
                returnErrors(
                    err.response.data,
                    err.response.status
                )
            )
        )
}

// Add Employee:
export const addEmployee = newEmployee => (dispatch, getState) => {
    // Call helper func to set the config
    const token = tokenConfig(getState)

    axios.post('/api/employees/', newEmployee, token)
        .then(res => {
            dispatch(
                createMessage(
                    { addEmployee: 'New Employee Added' }
                )
            )
            dispatch({
                type: ADD_EMPLOYEE,
                payload: res.data
            })
        })
        .catch(err =>
            dispatch(
                returnErrors(
                    err.response.data,
                    err.response.status
                )
            )
        )
}

// Return Employee Info:
export const getEmployeeInfo = employeeInfo => dispatch => {
    dispatch({
        type: EMPLOYEE_INFO,
        payload: employeeInfo
    })
}