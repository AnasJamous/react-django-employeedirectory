import React, {Component, Fragment} from 'react'
import ReactDOM from 'react-dom'
import { HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom'

// Redux Imports
import { Provider } from 'react-redux'
import store from '../store'

// My Components Imports
import Header from './layout/Header'
import Alerts from './layout/Alerts'
import Login from './accounts/Login'
import NotFound from './common/NotFound'
import Register from './accounts/Register'
import Dashboard from './employees/Dashboard'
import PrivateRoute from './common/PrivateRoute'
import UpdateEmployee from './employees/UpdateEmployee'

// React Alert Imports
import { Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'
import { loadUser } from '../actions/auth'

// Alert Options:
const alertOptions = {
    timeout: 3000,
    position: 'top center'
}

class App extends Component {
    componentDidMount() {
        store.dispatch(loadUser())
    }

    render() {
        return (
            <Provider store={store}>
                <AlertProvider template={ AlertTemplate } { ...alertOptions }>
                    <Router>
                        <div className='container-fluid'>
                            <Header />
                            <Alerts />
                            <div className='container'>
                                <Switch>
                                    <PrivateRoute exact path='/' component={Dashboard} />
                                    <PrivateRoute exact path='/employee/:empId' component={UpdateEmployee} />
                                    <Route exact path='/register' component={Register} />
                                    <Route exact path='/login' component={Login} />
                                    <Route component={NotFound} />
                                </Switch>
                            </div>
                        </div>
                    </Router>
                </AlertProvider>
            </Provider>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('app'))