import React, { Component } from 'react';
import { Redirect, withRouter  } from 'react-router-dom'

import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { deleteEmployee, deleteEmployeeVisually, getEmployeeInfo } from '../../actions/employees';


class BtnCellRenderer extends Component {
    constructor(props) {
        super(props);
        this.deleteAction = this.deleteAction.bind(this);
        this.updateEmployee = this.updateEmployee.bind(this);
    }

    componentDidMount() {
    }

    static propTypes = {
        deleteEmployee: PropTypes.func.isRequired,
        deleteEmployeeVisually: PropTypes.func.isRequired,
        getEmployeeInfo: PropTypes.func.isRequired,
    }

    deleteAction() {
        const selectedRow = this.props.api.getSelectedRows();
        const res = this.props.api.applyTransaction({ remove: selectedRow });

        // Handle Delete Employee from Mock Data:
        'owner' in this.props.data ?
        this.props.deleteEmployee(selectedRow[0].id) :
        this.props.deleteEmployeeVisually()
    }

    updateEmployee() {
        const empInfo = this.props.data
        this.props.getEmployeeInfo(empInfo)
        const empID = this.props.data.id
        let path = `/employee/${empID}`;
        this.props.history.push(path);
    }

    render() {

        return (
            <span>
                <button
                    style={{ height: 24, lineHeight: 0.5 }}
                    onClick={this.deleteAction}
                    className="btn btn-outline-danger mr-1"
                >
                    Delete
                </button>
                <button
                    style={{ height: 24, lineHeight: 0.5 }}
                    onClick={this.updateEmployee}
                    className="btn btn-outline-success"
                >
                    Update
                </button>
            </span>
        )
    }
}

export default connect(
    null,
    {
        deleteEmployee,
        deleteEmployeeVisually,
        getEmployeeInfo
    })
    (withRouter(BtnCellRenderer))