import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'


/*
This is kind of proxy for a regular routes except we wanna check the user
is logged in.
Props parameters to this function:
component : which will be the component that passed in
auth: from Redux,
...rest: for other props that will be added
*/

const PrivateRoute = ( { component: Component, auth, ...rest } ) => (
    <Route
        {...rest}
        render= { props => {
             if(auth.isLoading) {
                return <h2> Loading ... </h2>
             } else if(!auth.isAuthenticated) {
                return  <Redirect to="/login" />
             }
             return <Component {...props} />
         }}
    />
)

const mapStateToProps = state => ({
    auth: state.auth
})

export default connect(mapStateToProps)(PrivateRoute)