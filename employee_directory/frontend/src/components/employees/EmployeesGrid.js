import React, { Component } from 'react';
import PropTypes from 'prop-types'

// Redux Imports
import { connect } from 'react-redux'
import { getEmployees, getRandomEmployees} from '../../actions/employees'

// Ag-Grid Imports
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

// Button Component
import BtnCellRenderer from '../common/BtnCellRenderer'

// CountUp Component
import AnimatedCountUp from '../layout/AnimatedCountUp';

// Loader Component
import AnimatedLoader from '../layout/AnimatedLoader';


class EmployeesGrid extends Component {
     // Ag-Grid Variables for Its API
     gridApi;
     gridColumnApi;

     state = {
        employeesNumberAsProps: 0,
        employeesNumber: 0,
        randomEmployeesNumber: 0,
        columnDefs: [
            { headerName: 'ID', field: 'id', maxWidth: 90 },
            { headerName: 'Names', field: 'name' },
            { headerName: 'Job Title', field: 'job_title' },
            { headerName: 'Email', field: 'email', minWidth: 250 },
            { headerName: 'Bio', field: 'bio', filter:false, sortable: false },
            {
                headerName: 'Action',
                field: 'action',
                filter:false,
                sortable: false,
                cellRenderer: 'btnCellRenderer',
            }
        ],
        defaultColDef: {
            flex: 1,
            minWidth: 150,
            filter: true,
            resizable: true,
            sortable: true,
        },
        paginationPageSize: 10,
        paginationNumberFormatter: function(params) {
            return '[' + params.value.toLocaleString() + ']';
        },
        context: { componentParent: this },
        frameworkComponents: {
            btnCellRenderer: BtnCellRenderer,
        },
        rowSelection: 'single',
        rowData: []
     }

    // New Life-Cycle method replaced (componentWillReceiveProps):
    static getDerivedStateFromProps(props, state) {
        const formattedRandomEmployees = []

        // Formatting the random employees API to fit the table header names
        props.randomEmployees.map(employee => {
            formattedRandomEmployees.push({
                'id' : employee.location.street.number,
                'name': employee.name.first.concat(employee.name.last),
                'email': employee.email,
                'job_title': 'Web Developer',
                'bio': 'test'
            })
          })

        // Check if the Async API call finished, Otherwise state return null
        if ( props.employees.length ) {
          let columnDefs = []
          // Fill the Column Dynamically with the right names coming from the API:
            //Object.keys(props.employees[0]).forEach( key => {
            //    if (key !== 'created_at') {
            //        columnDefs.push({
            //            headerName: key,
            //            field: key
            //        })
            //    }
            //  })
          // Updating the state and return the new one
          // state.columnDefs = [...state.columnDefs, ...columnDefs]
          state.employeesNumber = props.employees.length
          state.randomEmployeesNumber = formattedRandomEmployees.length
          state.employeesNumberAsProps = state.employeesNumber + state.randomEmployeesNumber

          state.rowData = [...formattedRandomEmployees, ...props.employees ]
          return state
          } else if (props.employees.length === 0) {
                state.employeesNumberAsProps = formattedRandomEmployees.length
                state.rowData = formattedRandomEmployees
                return state
          }
        // No state update necessary
        return null;
    }

    static propTypes = {
        employees: PropTypes.array.isRequired,
        randomEmployees: PropTypes.array.isRequired,
        getEmployees: PropTypes.func.isRequired,
        getRandomEmployees: PropTypes.func.isRequired
    }

    componentDidMount() {
        this.props.getEmployees()
        this.props.getRandomEmployees()
    }

    onGridReady = (params) => {
        this.gridApi = params.api;
        this.gridColumnApi = params.api;
        params.api.sizeColumnsToFit();
    }

    // Handle the pagination size
    onPageSizeChanged = () => {
        const value = document.getElementById('page-size').value;
        this.gridApi.paginationSetPageSize(Number(value));
    };

    render() {
        const paginationWrapper = (
            <div className="example-header mb-2">
                Page Size:
                <select className="ml-2" onChange={() => this.onPageSizeChanged()} id="page-size">
                  <option value="10" defaultValue="">10</option>
                  <option value="100">100</option>
                  <option value="500">500</option>
                  <option value="1000">1000</option>
                </select>
            </div>
        )

        return (
            this.state.randomEmployeesNumber === 0   ?
            <AnimatedLoader />  : (
            <div
                className="ag-theme-alpine col-12 mx-auto col-md-12 col-sm-12"
                id="myGrid"
                style={{
                    height: '545px',
                    width: '100%',
                  }}
                >
                    <h1 className='col-12 mx-auto col-md-12 text-center mb-4 mt-4'>Employees List</h1>
                    <AnimatedCountUp employeesNumber={this.state.employeesNumberAsProps}/>
                {paginationWrapper}
                <AgGridReact
                    pagination={true}
                    rowData={this.state.rowData}
                    context={this.state.context}
                    onGridReady={this.onGridReady}
                    columnDefs={this.state.columnDefs}
                    rowSelection={this.state.rowSelection}
                    defaultColDef={this.state.defaultColDef}
                    paginationPageSize={this.state.paginationPageSize}
                    frameworkComponents={this.state.frameworkComponents}
                    paginationNumberFormatter={this.state.paginationNumberFormatter}
                >
                </AgGridReact>
            </div>
        ));
    }
}

const mapStateToProps = ( state ) => ({
    // the state.employees --> means that we want the reducer of name employees
    // AND state.employees.employees --> means I want the state name employees from employees reducer
    // employees key can be any name
    employees: state.employees.employees,
    randomEmployees: state.employees.randomEmployees
})

export default connect(mapStateToProps, {getEmployees, getRandomEmployees})(EmployeesGrid)