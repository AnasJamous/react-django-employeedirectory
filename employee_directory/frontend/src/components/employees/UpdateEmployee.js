import React, {Component, Fragment} from 'react'
import PropTypes from 'prop-types'

// Redux Imports
import { connect } from 'react-redux'
import { updateEmployee, UpdateEmployeeVisually } from '../../actions/employees'


export class UpdateEmployee extends Component{

    static propTypes = {
        updateEmployee: PropTypes.func.isRequired,
        UpdateEmployeeVisually: PropTypes.func.isRequired
    }

    state= {
        name: '',
        email: '',
        job_title: '',
        bio: ''
    }

    componentDidMount() {
        this.setState({
            name : this.props.employeeInfo.name,
            email : this.props.employeeInfo.email,
            job_title : this.props.employeeInfo.job_title,
            bio : this.props.employeeInfo.bio
        })
    }

    onChange = (e) => this.setState({ [e.target.name]: e.target.value });

    onSubmit = (e) => {
        e.preventDefault()
        const id = this.props.employeeInfo.id
        const { name, email, job_title, bio } = this.state
        const updatedEmployee = { name, email, job_title, bio }

        // Check if the updated user in DB or from Mock API:
        'owner' in this.props.employeeInfo ?
        this.props.updateEmployee(id, updatedEmployee) :
        this.props.UpdateEmployeeVisually()

        console.log(this.props)
        // Redirect to home page
         this.props.history.push('/');
    }


    render(){
        const { name, email, job_title, bio } = this.state

        return(
             <div className="col-12 mx-auto col-md-12 col-sm-12" style={{marginTop: '5em', marginBottom: '5em'}}>
                 <div className="card card-body">
                    <h1 className='text-center'>Update Employee</h1>
                    <form onSubmit={this.onSubmit}>
                      <div className="form-group">
                        <label>Name</label>
                        <input
                          className="form-control"
                          type="text"
                          name="name"
                          onChange={this.onChange}
                          value={name}
                        />
                      </div>
                      <div className="form-group">
                        <label>Email</label>
                        <input
                          className="form-control"
                          type="email"
                          name="email"
                          onChange={this.onChange}
                          value={email}
                        />
                      </div>
                      <div className="form-group">
                        <label>Job Title</label>
                        <textarea
                          className="form-control"
                          type="text"
                          name="job_title"
                          onChange={this.onChange}
                          value={job_title}
                        />
                      </div>
                      <div className="form-group">
                        <label>About</label>
                        <textarea
                          className="form-control"
                          type="text"
                          name="bio"
                          onChange={this.onChange}
                          value={bio}
                        />
                      </div>
                      <div className="form-group">
                        <button type="submit" className="btn btn-primary">
                          Submit
                        </button>
                      </div>
                    </form>
                  </div>
              </div>
        )
    }
}

const mapStateToProps = (state) => ({
    employeeInfo: state.employees.employeeInfo
})

export default connect(mapStateToProps,{ updateEmployee, UpdateEmployeeVisually })(UpdateEmployee)