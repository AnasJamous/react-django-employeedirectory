import React, {Fragment} from 'react'

import EmployeeForm from './EmployeeForm'
import EmployeesGrid from './EmployeesGrid'

export default function Dashboard() {
    return (
        <div>
            <EmployeesGrid />
            <EmployeeForm />
        </div>
    )
}

