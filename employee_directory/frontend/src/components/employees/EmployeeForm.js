import React, {Component, Fragment} from 'react'
import PropTypes from 'prop-types'

// Redux Imports
import { connect } from 'react-redux'
import { addEmployee } from '../../actions/employees'


export class EmployeeForm extends Component{

    static propTypes = {
        addEmployee: PropTypes.func.isRequired
    }

   state= {
        name: '',
        email: '',
        job_title: '',
        bio: ''
    }

    onChange = (e) => this.setState({ [e.target.name]: e.target.value });

    onSubmit = (e) => {
        e.preventDefault()
        const { name, email, job_title, bio } = this.state
        const newEmployee = { name, email, job_title, bio }
        this.props.addEmployee(newEmployee)

        this.setState({
            name: '',
            email: '',
            job_title: '',
            bio: ''
        })
    }


    render(){
        const { name, email, job_title, bio } = this.state

        return(
             <div className="col-12 mx-auto col-md-12 col-sm-12" style={{marginTop: '25em', marginBottom: '5em'}}>
                 <div className="card card-body">
                    <h1 className='text-center'>Add Employee</h1>
                    <form onSubmit={this.onSubmit}>
                      <div className="form-group">
                        <label>Name</label>
                        <input
                          className="form-control"
                          type="text"
                          name="name"
                          onChange={this.onChange}
                          value={name}
                        />
                      </div>
                      <div className="form-group">
                        <label>Email</label>
                        <input
                          className="form-control"
                          type="email"
                          name="email"
                          onChange={this.onChange}
                          value={email}
                        />
                      </div>
                      <div className="form-group">
                        <label>Job Title</label>
                        <textarea
                          className="form-control"
                          type="text"
                          name="job_title"
                          onChange={this.onChange}
                          value={job_title}
                        />
                      </div>
                      <div className="form-group">
                        <label>About</label>
                        <textarea
                          className="form-control"
                          type="text"
                          name="bio"
                          onChange={this.onChange}
                          value={bio}
                        />
                      </div>
                      <div className="form-group">
                        <button type="submit" className="btn btn-primary">
                          Submit
                        </button>
                      </div>
                    </form>
                  </div>
              </div>
        )
    }
}



// NOTE: here we don't pass mapStateToProps because here we don't want to bring back the state
// after the changes so we can put null
export default connect(null,{ addEmployee })(EmployeeForm)