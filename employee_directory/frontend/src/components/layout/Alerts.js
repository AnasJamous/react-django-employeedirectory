import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'

import { withAlert } from 'react-alert'

// Redux Imports
import { connect } from 'react-redux'


export class Alerts extends Component {
    static propTypes = {
        error: PropTypes.object.isRequired,
        message: PropTypes.object.isRequired,
    }

    componentDidUpdate(prevProps) {
        const { error, alert, message } = this.props
        if(error !== prevProps.error) {
            // The Error msg received as an Array form the server so using join turn it into a String
            if (error.msg.name) alert.error(`Name: ${error.msg.name.join()}`);
            if (error.msg.email) alert.error(`Email: ${error.msg.email.join()}`);
            if (error.msg.job_title) alert.error(`Job Title: ${error.msg.job_title.join()}`);
            if (error.msg.non_field_errors) alert.error(error.msg.non_field_errors.join());
            if (error.msg.username) alert.error(error.msg.username.join());
            if (error.msg.password) alert.error(error.msg.password.join());
        }

        if (message !== prevProps.message) {
            if (message.deleteEmployee) alert.success(message.deleteEmployee);
            if (message.addEmployee) alert.success(message.addEmployee);
            if (message.passwordNotMatch) alert.error(message.passwordNotMatch);
            if (message.updateEmployee) alert.success(message.updateEmployee);
        }
    }

    render() {
        return <Fragment />
    }
}


const mapStateToProps = state => ({
    error: state.errors,
    message: state.messages
})

export default connect(mapStateToProps)(withAlert()(Alerts));