import React from 'react';

// react-loader-spinner
import Loader from 'react-loader-spinner';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

const AnimatedLoader = () => {
    const visibility = true
    return(
        <div className='text-center mt-4'>
            <Loader
                 type="Rings"
                 color="#708192"
                 height={300}
                 width={300}
                 secondaryColor="grey"
                 visible = {visibility}
            />
        </div>
    );
}

export default AnimatedLoader