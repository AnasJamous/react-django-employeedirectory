import React, { useEffect } from 'react'
import CountUp from 'react-countup';
import { useCountUp } from 'react-countup';

const AnimatedCountUp = ({ employeesNumber }) => {

  const { countUp, start, update } = useCountUp({
    start: 0,
    end: employeesNumber,
    duration: 2.75,
    startOnMount:false
  });

  useEffect(()=> {
    if (employeesNumber > 0){
     start()
     update(employeesNumber)
    }
  }, [ employeesNumber ])

  return (
    <div className='col-12 mx-auto col-md-12 text-center mb-4 mt-4'>
            <h1>{countUp}</h1>
            <svg width="2.5em" height="2.5em" viewBox="0 0 16 16" className="bi bi-people-fill" fillRule="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fillRule="evenodd" d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5.784 6A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216zM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>
            </svg>
    </div>
  );
};

export default AnimatedCountUp