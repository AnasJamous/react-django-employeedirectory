import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'

// Redux Imports
import { connect } from 'react-redux'
import { userLogin } from '../../actions/auth'

export class Login extends Component{
    state = {
        username:'',
        password:'',
    }

    static proptypes = {
        userLogin: PropTypes.func.isRequired,
        isAuthenticated: PropTypes.bool
    }

    onSubmit = e => {
        e.preventDefault()
        this.props.userLogin(this.state.username, this.state.password)
    }

    onChange = e => this.setState({ [e.target.name] : e.target.value })

    render() {
        const { username, password } = this.state
        const { isAuthenticated } = this.props

        return (
           // Check if the user is authenticated
           isAuthenticated  ?
           <Redirect to='/' />  :
             (
              <div className="col-md-6 m-auto">
                <div className="card card-body mt-5">
                  <h2 className="text-center">Login</h2>
                  <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                      <label>Username</label>
                      <input
                        type="text"
                        className="form-control"
                        name="username"
                        onChange={this.onChange}
                        value={username}
                      />
                    </div>
                    <div className="form-group">
                      <label>Password</label>
                      <input
                        type="password"
                        className="form-control"
                        name="password"
                        onChange={this.onChange}
                        value={password}
                      />
                    </div>
                    <div className="form-group">
                      <button type="submit" className="btn btn-primary">
                        Login
                      </button>
                    </div>
                    <p>
                      Do not have an account? <Link to="/Register">Register</Link>
                    </p>
                  </form>
                </div>
              </div>
        ));
    }
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated
})

// THE SECOND PARAMETER BESIDE THE MapToProps IS THE ACTION WE ARE USING:
export default connect(mapStateToProps, {userLogin})(Login)