from rest_framework import serializers
from django.contrib.auth.models import User
from django.contrib.auth import authenticate


# User Serializer
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')


# Register Serializer
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
        # extra_kwargs allowing to add extra fields to the default model
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_date):
        user = User.objects.create_user(
            validated_date['username'],  # custom field-level validation by adding .validate_<field_name>
            validated_date['email'],  # custom field-level validation by adding .validate_<field_name>
            validated_date['password']  # custom field-level validation by adding .validate_<field_name>
        )

        return user


# Login Serializer
'''
     We didn't pass ModelSerializer because here we are not dealing with
     creating a model, otherwise we simply validating user if he is authenticated
'''


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        user = authenticate(**data)

        # Check if the user is active:
        if user and user.is_active:
            return user
        raise serializers.ValidationError('Incorrect Credentials')
